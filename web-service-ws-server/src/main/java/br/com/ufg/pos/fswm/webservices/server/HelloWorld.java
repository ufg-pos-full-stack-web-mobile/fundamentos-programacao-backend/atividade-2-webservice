package br.com.ufg.pos.fswm.webservices.server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface HelloWorld {

    @WebMethod String getHelloWorldAsString(String name);
}
