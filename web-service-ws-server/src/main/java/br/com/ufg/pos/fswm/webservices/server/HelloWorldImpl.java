package br.com.ufg.pos.fswm.webservices.server;

import javax.jws.WebService;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
@WebService(endpointInterface = "br.com.ufg.pos.fswm.webservices.server.HelloWorld")
public class HelloWorldImpl implements HelloWorld{

    public String getHelloWorldAsString(String name) {
        return "Hello World JAX-WS " + name;
    }

}
