package br.com.ufg.pos.fswm.webservices.server;

import javax.xml.ws.Endpoint;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 10/06/17.
 */
public class HelloWorldPublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/hello", new HelloWorldImpl());
    }
}
